# Corso di Formazione Excel 2021 Mar-Apr #

Corso di formazione sull'utilizzo di Excel da livello base fino a livello avanzato. 

Sono disponibili le [Slide](https://bitbucket.org/NiccoBiondi/excel/src/main/Slide/) esposte e i [Documenti](https://bitbucket.org/NiccoBiondi/excel/src/main/Documenti/) utilizzati durante il corso.
